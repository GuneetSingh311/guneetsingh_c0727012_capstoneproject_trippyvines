//
//  ViewController.swift
//  TrippyVines
//
//  Created by Guneet on 2019-04-04.
//  Copyright © 2019 Guneet. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import SpriteKit
import AVFoundation
class ViewController: UIViewController, ARSCNViewDelegate {
   
    @IBOutlet var sceneView: ARSCNView!
    @IBOutlet weak var recording: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Set the view's delegate
        sceneView.delegate = self
        // Show statistics such as fps and timing information
        sceneView.showsStatistics = true
        // Create a new scene
        let scene = SCNScene(named: "art.scnassets/ship.scn")!
        // Set the scene to the view
        sceneView.scene = scene
        self.recording.layer.cornerRadius = self.recording.frame.height/2
}
    
override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = .horizontal
        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Pause the view's session
        sceneView.session.pause()
}

    // MARK: - ARSCNViewDelegate
    // MARK: Tosuches began on screen
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {return}
        let result = sceneView.hitTest(touch.location(in: sceneView), types: ARHitTestResult.ResultType.estimatedHorizontalPlane)
        guard let hitresult = result.last else {return}
        let hittransform = SCNMatrix4(hitresult.worldTransform)
        let hitVector = SCNVector3Make(hittransform.m41, hittransform.m42, hittransform.m43)
        createVideo(position: hitVector)
}
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {return}
        let result = sceneView.hitTest(touch.location(in: sceneView), types: ARHitTestResult.ResultType.estimatedHorizontalPlane)
        guard let hitresult = result.last else {return}
        let hittransform = SCNMatrix4(hitresult.worldTransform)
        let hitVector = SCNVector3Make(hittransform.m41, hittransform.m42, hittransform.m43)
        createVideo(position: hitVector)
    }
    
    
    // MARK: CREATE EFFECT VIDEO SCENE
    func  createVideo(position: SCNVector3) {
    guard let container = sceneView.scene.rootNode.childNode(withName: "container", recursively: false) else {
        print("noimage")
        return
}
    container.removeFromParentNode()
    container.position = position
    container.geometry?.firstMaterial?.transparency = 0.00000001
    sceneView.scene.rootNode.addChildNode(container)
    container.isHidden = false
     var videoScene = SKScene(size: CGSize(width:1080 , height:720))
     var codeNode = SKSpriteNode(imageNamed: "cat.gif")
        let frame0 = SKTexture.init(imageNamed: "frame_00_delay-0.03s.gif")
        let frame1 = SKTexture.init(imageNamed: "frame_01_delay-0.03s.gif")
        let frame2 = SKTexture.init(imageNamed: "frame_02_delay-0.03s.gif")
        let frame3 = SKTexture.init(imageNamed: "frame_03_delay-0.03s.gif")
        let frame4 = SKTexture.init(imageNamed: "frame_04_delay-0.03s.gif")
        let frame5 = SKTexture.init(imageNamed: "frame_05_delay-0.03s.gif")
        let frame6 = SKTexture.init(imageNamed: "frame_06_delay-0.03s.gif")
        let frame7 = SKTexture.init(imageNamed: "frame_07_delay-0.03s.gif")
        let frame8 = SKTexture.init(imageNamed: "frame_08_delay-0.03s.gif")
        let frame9 = SKTexture.init(imageNamed: "frame_09_delay-0.03s.gif")
        let frame10 = SKTexture.init(imageNamed: "frame_10_delay-0.03s.gif")
        let frame11 = SKTexture.init(imageNamed: "frame_11_delay-0.03s.gif")
        let music = SKAction.playSoundFileNamed("trance.mp3", waitForCompletion: false)
        let catFrames: [SKTexture] = [frame0, frame1,frame2,frame3,frame4,frame5,frame6,
        frame7,frame8,frame9,frame10,frame11]
        let animation = SKAction.animate(with: catFrames, timePerFrame: 0.01)
     codeNode.position = CGPoint(x: videoScene.size.width/2, y: videoScene.size.height/2)
     codeNode.size = videoScene.size
     codeNode.yScale = -1
      codeNode.run(music)
     codeNode.run(music)
     codeNode.run(SKAction.repeatForever(animation))
     videoScene.addChild(codeNode)
     guard let video = container.childNode(withName: "video",
                                          recursively:false)
        else {
            return

    }
    video.geometry?.firstMaterial?.diffuse.contents = videoScene
}
    
    
 
/*
    // Override to create and configure nodes for anchors added to the view's session.
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        let node = SCNNode()
     
        return node
    }
*/
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }

}
